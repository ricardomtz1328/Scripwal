Param(
	[switch] $help      = $false,
	[switch] $developer = $true
)

Set-StrictMode -Version Latest

if ($help)
{
	Write-Host "Usage: .\setup.ps1 [-help] [-developer]"
	Write-Host ""
	Write-Host "Options:"
	Write-Host "      -help  Show help"
	Write-Host " -developer  Extra software developer setup (default: false)"
	exit
}

# Check for administrative privileges.
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
	Write-Host "Please run the script as an administrator."
	exit 1
}

# Common module.
. "$PSScriptRoot\common.ps1"

# Mount extra hives.
New-PSDrive -PSProvider Registry -Name HKCR -Root HKEY_CLASSES_ROOT -ErrorAction SilentlyContinue | Out-Null
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS -ErrorAction SilentlyContinue | Out-Null
reg load HKLM\Default "C:\Users\Default\NTUSER.DAT" 2> $null | Out-Null

# For modifying both the current and default user.
function SetKeyDefault([string]$path, [string]$name, $value, [string]$type = "String")
{
	SetKey "HKCU:\$path" $name $value $type
	SetKey "HKLM:\Default\$path" $name $value $type
}

function DeleteKeyDefault([string]$path, [bool]$recurse = $true)
{
	DeleteKey "HKCU:\$path" $recurse
	DeleteKey "HKLM:\Default\$path" $recurse
}

function DeleteItemDefault([string]$path, [string]$name)
{
	DeleteItem "HKCU:\$path" $name
	DeleteItem "HKLM:\Default\$path" $name
}

# Prepare the NuGet provider.
Install-PackageProvider -Name NuGet -Force | Out-Null

# Become TrustedInstaller to be able to access some restricted registry keys.
if (!(Get-Module -ListAvailable -Name NtObjectManager))
{
	Install-Module -Name NtObjectManager -Force -Scope "AllUsers" | Out-Null
}

Import-Module NtObjectManager

if (!(Get-Module -Name NtObjectManager))
{
	Write-Error "NtObjectManager missing. Allow it manually if Windows Defender blocked its installation."
}

Start-Service TrustedInstaller
$ti_process = Get-NtProcess -ServiceName "TrustedInstaller"
$ti_thread = $ti_process.GetFirstThread()
$current_thread = Get-NtThread -Current -PseudoHandle
$impersonation = $current_thread.ImpersonateThread($ti_thread)

# Remove unnecessary provisioned packages.
$packages = @(
	'AppUp.IntelGraphicsExperience',
	'Clipchamp.Clipchamp',
	'Microsoft.549981C3F5F10',
	'Microsoft.BingNews',
	'Microsoft.BingWeather',
	'Microsoft.GamingApp',
	'Microsoft.GetHelp',
	'Microsoft.Getstarted',
	'Microsoft.MicrosoftOfficeHub',
	'Microsoft.MicrosoftSolitaireCollection',
	'Microsoft.MicrosoftStickyNotes',
	'Microsoft.OneDriveSync',
	'Microsoft.People',
	'Microsoft.PowerAutomateDesktop',
	'Microsoft.ScreenSketch',
	'Microsoft.Todos',
	'Microsoft.windowscommunicationsapps',
	'Microsoft.WindowsFeedbackHub',
	'Microsoft.WindowsMaps',
	'Microsoft.WindowsSoundRecorder',
	'Microsoft.Xbox.TCUI',
	'Microsoft.XboxGameOverlay',
	'Microsoft.XboxGamingOverlay',
	'Microsoft.XboxIdentityProvider',
	'Microsoft.XboxSpeechToTextOverlay',
	'Microsoft.YourPhone',
	'Microsoft.ZuneMusic',
	'Microsoft.ZuneVideo',
	'MicrosoftCorporationII.QuickAssist',
	'MicrosoftTeams',
	'MicrosoftWindows.Client.WebExperience',
	'NVIDIACorp.NVIDIAControlPanel'
)

Import-Module Appx
Get-AppxProvisionedPackage -Online | Where-Object { $packages.Contains($_.DisplayName) } | Remove-AppxProvisionedPackage -Online | Out-Null
Get-AppxPackage                    | Where-Object { $packages.Contains($_.Name) }        | Remove-AppxPackage | Out-Null

# Set Estonia timezone
Set-TimeZone -Name "FLE Standard Time"
Start-Service W32Time
w32tm /resync | Out-Null

# Remove OneDrive
$onedrive = Get-ItemProperty -LiteralPath "HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall\OneDriveSetup.exe" -Name "UninstallString" -ErrorAction SilentlyContinue

if ($onedrive)
{
	Invoke-Expression $onedrive.UninstallString
}

# Replace the bad Windows 11 emoji font with Google Noto Color Emoji.
Download "https://tambre.ee/fonts/seguiemj.ttf"
Download "https://tambre.ee/fonts/seguisym.ttf"
RemoveFont "seguiemj.ttf"
RemoveFont "seguisym.ttf"
AddFont "files/seguiemj.ttf"
AddFont "files/seguisym.ttf"

# Remove shortcut indicator arrows.
CopyItem "$PSScriptRoot\blank.ico" "C:\Windows\System32\"
SetKey "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Icons" "29" "blank.ico,0"

# Always show scrollbars.
SetKeyDefault "Control Panel\Accessibility" "DynamicScrollbars" 0 "DWORD"

# Disable sticky keys shortcut.
SetKeyDefault "Control Panel\Accessibility\StickyKeys" "Flags" "506"

# Default to 100% scaling.
SetKeyDefault "Control Panel\Desktop" "LogPixels" 96 "DWORD"

# Modify settings for the current, default and the logon users.
foreach ($prefix in @("HKCU:", "HKLM:\Default", "HKU:\.DEFAULT"))
{
	# Default geo to Estonia.
	SetKey "$prefix\Control Panel\International\Geo" "Name" "EE"
	SetKey "$prefix\Control Panel\International\Geo" "Nation" "70"

	# Estonian keyboard as the default.
	# TODO: It would be nicer if this worked in case there were more layouts.
	#       DeleteKey is crude as it would result in changes being reported on every run.
	# TODO: Modifying HKU:\.DEFAULT reverts to default on boot.
	DeleteItem "$prefix\Control Panel\International\User Profile\en-GB" "0809:00000809"
	DeleteItem "$prefix\Control Panel\International\User Profile System Backup\en-GB" "0809:00000809"
	DeleteItem "$prefix\Keyboard Layout\Preload" "2"
	DeleteItem "$prefix\Keyboard Layout\Substitutes" "d0010809"
	SetKey "$prefix\Control Panel\International\User Profile\en-GB" "0809:00000425" 1 "DWORD"
	SetKey "$prefix\Control Panel\International\User Profile System Backup\en-GB" "0809:00000425" 1 "DWORD"
	SetKey "$prefix\Keyboard Layout\Preload" "1" "00000809"
	SetKey "$prefix\Keyboard Layout\Substitutes" "00000809" "00000425"
	SetKey "$prefix\Software\Microsoft\CTF\Assemblies\0x00000809\{34745C63-B2F0-4784-8B67-5E12C8701A31}" "KeyboardLayout" 0x04250809 "DWORD"
	SetKey "$prefix\Software\Microsoft\CTF\SortOrder\AssemblyItem\00000425\{34745C63-B2F0-4784-8B67-5E12C8701A31}\00000000" "KeyboardLayout" 0x04250425 "DWORD"
	SetKey "$prefix\Software\Microsoft\CTF\SortOrder\AssemblyItem\00000425\{34745C63-B2F0-4784-8B67-5E12C8701A31}\00000000" "CLSID" "{00000000-0000-0000-0000-000000000000}"

	# Disable CTRL+SHIFT for switching languages.
	SetKey "$prefix\Keyboard Layout\Toggle" "Hotkey" "3"

	# Allow hex Unicode input using the numpad.
	SetKey "$prefix\Control Panel\Input Method" "EnableHexNumpad" "1"

	# Use ISO8601 for dates.
	SetKey "$prefix\Control Panel\International" "iDate" "2"
	SetKey "$prefix\Control Panel\International" "sDate" "-"
	SetKey "$prefix\Control Panel\International" "sShortDate" "yyyy-MM-dd"
}

# Don't show Office.com files.
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" "ShowCloudFilesInQuickAccess" 0 "DWORD"

# Reduce spacing.
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "UseCompactMode" 1 "DWORD"

# Default to "This PC"
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "LaunchTo" 1 "DWORD"

# Show hidden files and file extensions.
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "Hidden" 1 "DWORD"
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "HideFileExt" 0 "DWORD"

# Remove taskbar buttons.
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Search" "SearchboxTaskbarMode" 0 "DWORD"
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "ShowTaskViewButton" 0 "DWORD"
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "TaskbarDa" 0 "DWORD"
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "TaskbarMn" 0 "DWORD"

# Disable Bing in the search bar.
SetKeyDefault "SOFTWARE\Microsoft\Windows\CurrentVersion\Search" "DisableSearchBoxSuggestions" 1 "DWORD"

# Show "Allow Away Mode Policy" in power settings.
SetKey "HKLM:\SYSTEM\CurrentControlSet\Control\Power\PowerSettings\238C9FA8-0AAD-41ED-83F4-97BE242C8F20\25DFA149-5DD1-4736-B5AB-E8A37B5B8187" "Attributes" 2 "DWORD"

# Disable "Allow Away Mode Policy" for the current power profile.
$active_power_scheme = GetKey "HKLM:\SYSTEM\CurrentControlSet\Control\Power\User\PowerSchemes" "ActivePowerScheme"
SetKey "HKLM:\SYSTEM\CurrentControlSet\Control\Power\User\PowerSchemes\$active_power_scheme\238c9fa8-0aad-41ed-83f4-97be242c8f20\25dfa149-5dd1-4736-b5ab-e8a37b5b8187" "ACSettingIndex" 1 "DWORD"

# Revert to the old style context menu.
SetKey "HKLM:\SOFTWARE\CLASSES\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32" "(Default)" ""

# Remove context menu items.
DeleteKey "HKCR:\AllFilesystemObjects\shellex\ContextMenuHandlers\ModernSharing" # "Share"
DeleteKey "HKCR:\CLSID\{09A47860-11B0-4DA5-AFA5-26D86198A780}" # "Scan with Windows Defender..."
DeleteKey "HKCR:\Folder\ShellEx\ContextMenuHandlers\Library Location" # "Include in library"
DeleteKey "HKCR:\*\shellex\ContextMenuHandlers\{90AA3A4E-1CBA-4233-B8BB-535773D48449}" # "Pin to Taskbar"
DeleteKey "HKCR:\exefile\shellex\ContextMenuHandlers\Compatibility" # "Troubleshoot compatibility"

# Remove "Pin to Start" from the context menu.
DeleteKey "HKCR:\Folder\ShellEx\ContextMenuHandlers\PintoStartScreen"
DeleteKey "HKCR:\exefile\shellex\ContextMenuHandlers\PintoStartScreen"

# Remove "Restore previous versions" from the context menu.
DeleteKey "HKCR:\CLSID\{450D8FBA-AD25-11D0-98A8-0800361B1103}\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}"
DeleteKey "HKCR:\AllFilesystemObjects\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}"
DeleteKey "HKCR:\Directory\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}"

# Remove "Cast to Device" from the context menu.
SetKey "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Blocked" "{7AD84985-87B4-4a16-BE58-8B72A5B390F7}" ""

# Remove "Give access to" from the context menu.
DeleteKey "HKCR:\*\shellex\ContextMenuHandlers\Sharing"
DeleteKey "HKCR:\Directory\shellex\ContextMenuHandlers\Sharing"
DeleteKey "HKCR:\Directory\Background\shellex\ContextMenuHandlers\Sharing"
DeleteKey "HKCR:\Drive\shellex\ContextMenuHandlers\Sharing"

# Remove "Pin to Quick Access" from the context menu.
DeleteKey "HKCR:\Folder\shell\pintohome"

# Avoid duplicate removable drives in the Explorer.
DeleteKey "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Desktop\NameSpace\DelegateFolders\{F5FB2C77-0E2F-4A16-A381-3E560C68BC83}"

# Force Windows to save UTC into RTC instead of local time to avoid clock jumps when dual booting.
SetKey "HKLM:\SYSTEM\CurrentControlSet\Control\TimeZoneInformation" "RealTimeIsUniversal" 1 "DWORD"

# Save full crash dumps.
SetKey "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps" "DumpFolder" "B:\Dumps"
SetKey "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps" "DumpCount" 5 "DWORD"
SetKey "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps" "DumpType" 2 "DWORD"

while ($developer)
{
	if (!(Get-ChildItem -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" | Where-Object {
			try { ($_ | Get-ItemProperty -Name DisplayName -ErrorAction SilentlyContinue).DisplayName -Match "OpenSSH for Windows" }
			catch {}
		}) | Out-Null)
	{
		Write-Host "Please install OpenSSH for Windows from https://www.mls-software.com/opensshd.html"
		break
	}

	Write-Host "Applying custom profile..."

	# Install custom PowerShell profile.
	$profile_folder = "$env:USERPROFILE\Documents\PowerShell"
	CreateDirectory "$profile_folder\"
	CopyItem "$PSScriptRoot\profile.ps1" "$profile_folder\"

	# Switch to the non-official SSH distribution.
	Remove-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0 | Out-Null
	Install-Module -Name posh-sshell -Force -Scope "AllUsers" | Out-Null
	[System.Environment]::SetEnvironmentVariable("GIT_SSH", "$env:ProgramFiles\OpenSSH\bin\ssh.exe", "Machine")

	# cmd.exe profile to support ssh-agent with Visual Studio Code.
	CopyItem "$PSScriptRoot\profile.cmd" "$profile_folder\"
	SetKey "HKLM:\SOFTWARE\Microsoft\Command Processor" "AutoRun" "$env:USERPROFILE\Documents\PowerShell\profile.cmd"

	# Symlink to a folder that the user can access so the PID file can be written.
	if ((Get-Item $env:ProgramFiles\OpenSSH\tmp).LinkType -ne "SymbolicLink")
	{
		Remove-Item -Path $env:ProgramFiles\OpenSSH\tmp -Recurse
		New-Item -ItemType SymbolicLink -Path "$env:ProgramFiles\OpenSSH\tmp" -Target "$env:LOCALAPPDATA\Temp" | Out-Null
	}

	# Finally install posh-git.
	Install-Module -Name posh-git -Force -Scope "AllUsers" | Out-Null

	break
}

# Cleanup
reg unload HKLM\Default 2> $null
